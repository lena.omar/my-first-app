
import 'package:first_app/models/language.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AppLanguage extends Model {
  static final instance = AppLanguage();

  Locale _appLocale = Locale('en');

  Locale get appLocal => _appLocale ?? Locale('en');

  void changeLanguage(Language language){

    switch(language.languageCode){
      case "en":
        _appLocale=Locale(language.languageCode,'Us');
        break;
      case 'ar':
        _appLocale=Locale(language.languageCode,'SA');
        break;
      default:
        _appLocale=Locale(language.languageCode,'Us');
    }
    notifyListeners();
  }


}
