import 'package:flutter/material.dart';

class AppBarHelper {
  static var instance = AppBarHelper();

  AppBar createAppBar({
    @required BuildContext context,
    title = 'Screen Name',
    Color backgroundColor,
    List<Widget> actions,
    double evaluation = 0.0,
    Widget leading,
  }) {
    return AppBar(
      leading: leading,
      elevation: evaluation,
      actions: actions != null ? actions : [],
      centerTitle: true,
      title: Text(
        title.toString().replaceAll('\n', ''),
        textAlign: TextAlign.center,
        style: TextStyle(
            height: 1,
            fontSize: MediaQuery.of(context).size.width * 0.04,
            fontWeight: FontWeight.w600),
      ),
    );
  }
}
