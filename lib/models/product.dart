import 'package:flutter/material.dart';

class Product {
  String name = "";
  double price = 0;
  String img = '';
  String desc = '';
  int quantity = 0;

  Product(
      {@required this.name,
        @required this.price,
        @required this.img,
        @required this.desc,
        @required this.quantity});

  Product.map(int index,dynamic obj) {
    this.img = obj[index]['imagePath'];
    this.name = obj[index]['productName'];
    this.quantity = int.tryParse(obj[index]['quantity']);
    this.desc = obj[index]['description'];
    this.price = double.tryParse(obj[index]['price']);
  }
  String get getName=>name;
  String get getImage=>img;
  String get getDesc=>desc;
  int get getQuantity=>quantity;
  double get getPrice=>price;


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["imagePath"] = img;
    map["productName"] = name;
    map["quantity"] = quantity;
    map["description"] = desc;
    map["price"] = price;
    return map;
  }
} 