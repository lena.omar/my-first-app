import 'dart:ui';

import 'package:first_app/screens/extra/location_services.dart';
import 'package:first_app/utils/ui/navigation_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gifimage/flutter_gifimage.dart';

class ExtraScreen extends StatefulWidget {
  const ExtraScreen({Key key}) : super(key: key);

  @override
  _ExtraScreenState createState() => _ExtraScreenState();
}

class _ExtraScreenState extends State<ExtraScreen>
    with TickerProviderStateMixin {
  var _selectedTab = 0;
  GifController _gifController;

//AnimationController _animationController;
  @override
  void initState() {
    _gifController = GifController(
      vsync: this,
    );
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _gifController.repeat(
          min: 0, max: 16, period: Duration(milliseconds: 1500));
    });
    super.initState();
  }


  final Map<int, Widget> _childrenSegment = <int, Widget>{
    0: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text(
          'First',
          style: TextStyle(fontSize: 16),
        )),
    1: Padding(
        padding: EdgeInsets.all(8.0),
        child: Text('Second', style: TextStyle(fontSize: 16))),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            elevation: 2,
            backgroundColor: Theme.of(context).primaryColor,
            centerTitle: true,
            title: Text(
              "Extra Screen",
            ),
            bottom: PreferredSize(
                child: Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 12),
                  child: Row(
                    children: <Widget>[
                      SizedBox(width: 24),
                      Expanded(
                        child: CupertinoSegmentedControl(
                            children: _childrenSegment,
                            //pressedColor: Theme.of(context).primaryColor,
                            selectedColor: Colors.blue[700],
                            borderColor: Colors.white,
                            groupValue: this._selectedTab,
                            onValueChanged: (value) {
                              // TODO: - fix it
                              setState(() {
                                _selectedTab = value;
                              });
                            }),
                      ),
                      SizedBox(width: 24),
                    ],
                  ),
                ),
                preferredSize: Size(double.infinity, 48))),
        drawer: NavigationDrawer(),
        body: _selectedTab == 0
            ? LocationServices()
            : Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    GifImage(
                      controller: _gifController,
                      image: NetworkImage(
                          "https://houseofspices.co.nz/wp-content/themes/twentynineteen/images/loder.gif"),
                    ),
                    Text(
                      'Second segment control',
                      style: TextStyle(
                          fontSize: 28, color: Theme.of(context).primaryColor),
                    ),
                  ])));
  }
}
