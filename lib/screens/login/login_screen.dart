import 'package:first_app/models/language.dart';
import 'package:first_app/models/product.dart';
import 'package:first_app/screens/home/home_screen.dart';
import 'package:first_app/screens/signup/signup_screen.dart';
import 'package:first_app/services/api_service.dart';
import 'package:first_app/utils/helper/database_helper.dart';
import 'package:first_app/utils/helper/demo_data.dart';
import 'package:first_app/utils/helper/language_helper.dart';
import 'package:first_app/utils/helper/shared_prefrence_helper.dart';
import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:first_app/utils/ui/popup_helper.dart';
import 'package:flutter/material.dart';
import 'package:first_app/generated/l10n.dart';


class LogInScreen extends StatefulWidget {
  const LogInScreen({Key key}) : super(key: key);

  @override
  _LogInScreenState createState() => _LogInScreenState();
}

class _LogInScreenState extends State<LogInScreen> {
  TextEditingController _userNameController = TextEditingController();
  FocusNode _userNameFocusNode = FocusNode();
  TextEditingController _passwordController = TextEditingController();
  FocusNode _passwordFocusNode = FocusNode();
  bool _passwordObsureText = true;
  DatabaseHelper instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    instance = DatabaseHelper();
    //AppLanguage.shared.changeLanguage(Language.languageList()[0]);
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.instance.createAppBar(
          context: context,
          backgroundColor: Theme.of(context).primaryColor,
          title: S.current.logInScreen,
          actions: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: DropdownButton(
                onChanged: (Language language){
                  AppLanguage.instance.changeLanguage(language);
                },
                underline: SizedBox(),
                icon: Icon(Icons.language_outlined,color: Colors.white,),
                items: Language.languageList().map<DropdownMenuItem<Language>>(
                    (lang) => DropdownMenuItem(
                        child: Row(children: [
                          Text(lang.flag,style: TextStyle(fontSize: 20),),
                          Padding(padding: EdgeInsets.only(right: 8)),
                          Text(lang.name),
                        ]), value: lang)).toList(),
              ),
            ),
          ]),

      // AppBar(
      //   title: Text(
      //     "LogIn Screen ",
      //     style: TextStyle(
      //         height: 1,
      //         fontSize: MediaQuery.of(context).size.width * 0.04,
      //         fontWeight: FontWeight.normal),
      //   ),
      //   centerTitle: true,
      //   backgroundColor: Theme.of(context).primaryColor,
      //   actions: [
      //     Icon(Icons.power_settings_new_outlined),
      //   ],
      //   leading: Icon(Icons.arrow_back_outlined),
      // ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(16),
                child: Image.asset(
                  'assets/market.png',
                  width: 170,
                  height: 170,
                  fit: BoxFit.cover,
                ),
                // Image.network('https://picsum.photos/250?image=9')
              ),
              Text(
                S.current.welcomeMessage,
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 18,
                    //fontFamily: 'Lobster-Regular',
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 12,
              ),
              CommonUiViews.instance.createTextField(
                  context: context,
                  controller: _userNameController,
                  focusNode: _userNameFocusNode,
                  textStyle:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  textInputType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  hint: S.current.userNameHint,
                  label: S.current.userNameHint,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.person_outline),
                    color: Theme.of(context).primaryColor,
                    onPressed: () {},
                  ),
                  onSubmitted: () {
                    FocusScope.of(context).requestFocus(_passwordFocusNode);
                  }),
              CommonUiViews.instance.createTextField(
                  context: context,
                  controller: _passwordController,
                  focusNode: _passwordFocusNode,
                  textInputType: TextInputType.streetAddress,
                  textStyle:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 20),
                  textInputAction: TextInputAction.done,
                  hint: S.current.passwordHint,
                  label: S.current.passwordHint,
                  obscureText: _passwordObsureText,
                  suffixIcon: IconButton(
                    icon: Icon(_passwordObsureText
                        ? Icons.visibility_off_outlined
                        : Icons.visibility_outlined),
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      setState(() {
                        _passwordObsureText = !_passwordObsureText;
                      });
                    },
                  ),
                  onSubmitted: () {
                    FocusScope.of(context).requestFocus();
                  }),
              SizedBox(
                height: 12,
              ),
              CommonUiViews.instance.creatCustomButton(
                  context: context,
                  title: S.current.signInButton,
                  size: 20,
                  padding: 4,
                  onPressed: () async {
                    String read =
                        await SharedPrefranceHelper().read(key: 'isDataSaved');
                    if (read != 'true') saveDataToDatabase();
                    var response = await ApiService().postRequest(
                        context: context,
                        url: 'https://foryo.store/api/login',
                        body: {
                          "username": _userNameController.text,
                          "password": _passwordController.text
                        });
                    if (response.statusCode == 200)
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => HomeScreen()));
                    else {
                      PobUpHelper.instance.showAlertDialog(
                          context: context, message: response.body);
                    }
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    S.current.confirmAccount,
                    style: TextStyle(
                        color: Colors.black87, fontWeight: FontWeight.w600),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => SignUpScreen()));
                    },
                    child: Text(
                     S.current.register,
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.blueAccent),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool isFeildsValid() {
    return true;
  }

  void saveDataToDatabase() async {
    List<Product> allProducts = DemoData.products;
    for (var product in allProducts) {
      instance.saveProduct(product);
    }
    SharedPrefranceHelper().save(key: "isDataSaved", value: 'true');
    print("Data Saved Successfully");
  }
}
