// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addToCart" : MessageLookupByLibrary.simpleMessage("اضافة الى عربة التسوق"),
    "audio" : MessageLookupByLibrary.simpleMessage("الصوت"),
    "category" : MessageLookupByLibrary.simpleMessage("أنواع"),
    "categoryDrawer" : MessageLookupByLibrary.simpleMessage("أنواع"),
    "categoryScreen" : MessageLookupByLibrary.simpleMessage("شاشة الانواع"),
    "confirmAccount" : MessageLookupByLibrary.simpleMessage("ليس لديك حساب!"),
    "confirmPasswordHint" : MessageLookupByLibrary.simpleMessage(" تأكيد الرمز السري"),
    "emailHint" : MessageLookupByLibrary.simpleMessage("البريد الالكتروني"),
    "exploreAll" : MessageLookupByLibrary.simpleMessage("المزيد"),
    "home" : MessageLookupByLibrary.simpleMessage("الرئيسية"),
    "homeScreen" : MessageLookupByLibrary.simpleMessage("الشاشة الرئيسية"),
    "location" : MessageLookupByLibrary.simpleMessage("الموقع"),
    "logInScreen" : MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
    "logOut" : MessageLookupByLibrary.simpleMessage("الخروج"),
    "mobile" : MessageLookupByLibrary.simpleMessage("رقم الهاتف"),
    "more" : MessageLookupByLibrary.simpleMessage("المزيد"),
    "nameHint" : MessageLookupByLibrary.simpleMessage("الاسم"),
    "passwordHint" : MessageLookupByLibrary.simpleMessage("الرمز السري"),
    "register" : MessageLookupByLibrary.simpleMessage("تسجيل"),
    "showProducts" : MessageLookupByLibrary.simpleMessage("اظهر المنتجات"),
    "signInButton" : MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
    "signUpButton" : MessageLookupByLibrary.simpleMessage("تسجيل"),
    "signUpScreen" : MessageLookupByLibrary.simpleMessage("شاشة الاشتراك"),
    "topProducts" : MessageLookupByLibrary.simpleMessage("أهم المنتجات"),
    "userNameHint" : MessageLookupByLibrary.simpleMessage("اسم المستخدم"),
    "welcomeMessage" : MessageLookupByLibrary.simpleMessage("مرحبًا بك في تطبيق Market"),
    "yourCartIsEmpty" : MessageLookupByLibrary.simpleMessage("عربة التسوق فارغة")
  };
}
