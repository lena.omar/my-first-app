import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class RecordListView extends StatefulWidget {
  final List<String> records;

  const RecordListView({
    Key key,
    this.records,
    onSaved,
  }) : super(key: key);

  @override
  _RecordListViewState createState() => _RecordListViewState();
}

class _RecordListViewState extends State<RecordListView> {
  int _totalDuration;
  int _currentDuration;
  double _completedPercentage = 0.0;
  bool _isPlaying = false;

//   AudioPlayer advancedPlayer = AudioPlayer();
  int _selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return widget.records.isEmpty
        ? Center(child: Text('No records yet'))
        : ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: const AlwaysScrollableScrollPhysics(),
            reverse: false,
            itemBuilder: (BuildContext context, int i) {
              return Card(
                elevation: 5,
                child: ExpansionTile(
                  title: Text('New recoding ${i+1}'),
                  subtitle: Text(_getDateFromFilePath(
                      filePath: widget.records.elementAt(i))),
                  onExpansionChanged: ((newState) {
                    if (newState) {
                      setState(() {
                        _selectedIndex = i;
                      });
                    }
                  }),
                  children: [
                    Container(
                      height: 100,
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          LinearProgressIndicator(
                            minHeight: 5,
                            backgroundColor: Colors.black,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.green),
                            value:
                                _selectedIndex == i ? _completedPercentage : 0,
                          ),
                          IconButton(
                            icon: _selectedIndex == i
                                ? _isPlaying
                                    ? Icon(Icons.pause)
                                    : Icon(Icons.play_arrow)
                                : Icon(Icons.play_arrow),
                            onPressed: () => _onPlay(
                                filePath: widget.records.elementAt(i),
                                index: i),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
            itemCount: widget.records.length,
          );
  }

  Future<void> _onPlay({String filePath, int index}) async {
    AudioPlayer audioPlayer = AudioPlayer();

    if (!_isPlaying) {
      audioPlayer.play(filePath, isLocal: true);
      setState(() {
        _selectedIndex = index;
        _completedPercentage = 0.0;
        _isPlaying = true;
      });

      audioPlayer.onPlayerCompletion.listen((_) {
        setState(() {
          _isPlaying = false;
          _completedPercentage = 0.0;
        });
      });
      audioPlayer.onDurationChanged.listen((duration) {
        setState(() {
          _totalDuration = duration.inMicroseconds;
        });
      });

      audioPlayer.onAudioPositionChanged.listen((duration) {
        setState(() {
          _currentDuration = duration.inMicroseconds;
          _completedPercentage =
              _currentDuration.toDouble() / _totalDuration.toDouble();
        });
      });
    }
  }

  String _getDateFromFilePath({String filePath}) {

    DateTime recordedDate = DateTime.now();

    int year = recordedDate.year;
    int month = recordedDate.month;
    int day = recordedDate.day;
    String deto = '$year-$month-$day';
    return (deto);


  }
}
