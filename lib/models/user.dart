// import 'dart:convert';
//
// import 'package:flutter/material.dart';
//
// class User {
//   String _userName = '';
//   String _fullName = '';
//   String _password = '';
//   String _mobile = '';
//
//   User(this._userName, this._fullName, this._password, this._mobile,
//       this._confirmPassword);
//
//
//
//   String get userName => _userName;
//
//   String get fullName => _fullName;
//
//   String get mobile => _mobile;
//
//   String get password => _password;
//
//   User.fromJson(Map<String, dynamic> json) {
//     _userName = json['userName'];
//     _fullName = json['fullName'];
//     _mobile = json['mobile'];
//     _password = json['password'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = Map<String, dynamic>();
//     map['userName'] = this._userName;
//     map['fullName'] = this._fullName;
//     map['mobile'] = this._mobile;
//     map['password'] = this._password;
//     return map;
//   }
// }
//
// UserResponseModel userResponseModelFromJson(String str) =>
//     UserResponseModel.fromJson(json.decode(str));
//
// class UserResponseModel {
//   int code;
//   String message;
//
//   UserResponseModel(this.code, this.message);
//
//   UserResponseModel.fromJson(Map<String, dynamic> json) {
//     code = json['code'];
//     message = json['message'];
//   }
//
//   Map<String, dynamic> toJson() {
//     var map = Map<String, dynamic>();
//     map['code'] = this.code;
//     map['message'] = this.message;
//     return map;
//   }
// }
