import 'dart:io';

import 'package:first_app/utils/helper/list_sound_recorder.dart';
import 'package:first_app/utils/helper/sound_recodrer.dart';
import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class AudioScreen extends StatefulWidget {
  //final String _title;
  const AudioScreen({
    Key key,
  }) : super(key: key);

  @override
  _AudioScreenState createState() => _AudioScreenState();
}

class _AudioScreenState extends State<AudioScreen> {
  final recorder = SoundRecorder();
  Directory appDirectory;
  List<String> records = [];

  @override
  void initState() {
    super.initState();
    getApplicationDocumentsDirectory().then((value) {
      appDirectory = value;
      appDirectory.list().listen((onData) {
        if (onData.path.contains('.aac')) records.add(onData.path);
      }).onDone(() {
        records = records.reversed.toList();
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    appDirectory.delete();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarHelper.instance
          .createAppBar(context: context, title: "Audio Screen", actions: [
        IconButton(
          icon: Icon(Icons.remove_circle_outline),
          onPressed: _deleteAllRecords,
        //  onPressed: dispose,
        )
      ]),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.7,
            child: RecordListView(
              records: records
            ),
          ),
          // Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
          Expanded(
            flex: 2,
            child: SoundRecorder(
              onSaved: _onRecordComplete,
            ),
          ),
        ],
      ),
    );
  }

  _onRecordComplete() {
    records.clear();
    appDirectory.list().listen((onData) {
      records.add(onData.path);
    }).onDone(() {
      records.sort();
      records = records.reversed.toList();
      setState(() {});
    });
  }

  _deleteAllRecords() {
    setState(() {
      // appDirectory.list().listen((event) {}).onDone(() {
      //   records.clear();
      // });
      appDirectory.delete(recursive: true);
      records.clear();
    });

    // appDirectory.delete(recursive: true);
  }
}
