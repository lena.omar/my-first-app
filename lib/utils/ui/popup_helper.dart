import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

typedef void AlertHelperCompletionHendler();

class PobUpHelper {
  static var instance = PobUpHelper();

  void showAlertDialog(
      {String message,
      String button = 'ok',
      BuildContext context,
      AlertHelperCompletionHendler completionHendler}) {
    if (message == null || context == null) {
      return;
    }
    _showAndroidAlertDialog(
      message: message,
      button: button,
      context: context,
      completionHendler: completionHendler
    );
  }

  void showToast({
    String message,
    Toast toastLength = Toast.LENGTH_LONG,
    ToastGravity gravity = ToastGravity.TOP,
    @required BuildContext context,
  }) {
    if (message == null) {
      return;
    }
    Color backgroundColor = Colors.blue.shade900;
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: message,
      toastLength: toastLength,
      gravity: gravity,
      backgroundColor: backgroundColor,
      textColor: Colors.white,
      fontSize: 18.0,
    );
  }

  void showSuccessSnackBar(BuildContext context, String msg) {
    final snackBar = SnackBar(
      content: Container(
        height: 50,
        child: Center(
          child: Text(
            msg ?? '',
            style: TextStyle(color: Colors.white, fontSize: 20),
            textAlign: TextAlign.center,
          ),
        ),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Theme.of(context).primaryColor,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void showPobUpWithButtons(
      {@required BuildContext context,
      String title = "",
      String desc = "",
      String positiveLabel = "",
      String negativeLabel = "",
      Function() positinePressed,
      Function() negativePressed,
      TextStyle textStyle,
      AlertType type = AlertType.success}) {
    Alert(
      context: context,
      //type: AlertType.warning,
      title: title,

      desc: desc,

      buttons: [
        DialogButton(
          child: Text(
            negativeLabel.toString(),
            style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 20,
                fontWeight: FontWeight.w600),
          ),
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop();
            if (negativePressed != null) {
              negativePressed.call();
            }
          },
          color: Colors.transparent,
        ),
        DialogButton(
          child: Text(
            positiveLabel.toString(),
            style: TextStyle(
                color: Theme.of(context).accentColor,
                fontSize: 20,
                fontWeight: FontWeight.w600),
          ),
          onPressed: () {
            Navigator.of(context, rootNavigator: true).pop();
            if (positinePressed != null) {
              positinePressed.call();
            }
          },
          color: Colors.transparent,
        ),
      ],
    ).show();
  }

  void _showAndroidAlertDialog(
      {String message,
      String button = 'ok',
      BuildContext context,
      AlertHelperCompletionHendler completionHendler}) {
    if (message == null || context == null) {
      return;
    }
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(message),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    if(completionHendler!=null){
                      completionHendler();
                    }
                  },
                  child: Text(
                    button,
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ))
            ],
          );
        });
  }


}
