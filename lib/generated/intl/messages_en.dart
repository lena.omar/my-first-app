// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "addToCart" : MessageLookupByLibrary.simpleMessage("Add To Cart"),
    "audio" : MessageLookupByLibrary.simpleMessage("Audio"),
    "category" : MessageLookupByLibrary.simpleMessage("Categories"),
    "categoryDrawer" : MessageLookupByLibrary.simpleMessage("Category"),
    "categoryScreen" : MessageLookupByLibrary.simpleMessage("Category Screen"),
    "confirmAccount" : MessageLookupByLibrary.simpleMessage("you dont have an account!"),
    "confirmPasswordHint" : MessageLookupByLibrary.simpleMessage(" Confirm Password"),
    "emailHint" : MessageLookupByLibrary.simpleMessage("Email"),
    "exploreAll" : MessageLookupByLibrary.simpleMessage("Explore All"),
    "home" : MessageLookupByLibrary.simpleMessage("Home"),
    "homeScreen" : MessageLookupByLibrary.simpleMessage("Home Screen"),
    "location" : MessageLookupByLibrary.simpleMessage("Location"),
    "logInScreen" : MessageLookupByLibrary.simpleMessage("LogIn Screen"),
    "logOut" : MessageLookupByLibrary.simpleMessage("LogOut"),
    "mobile" : MessageLookupByLibrary.simpleMessage("Mobile Number"),
    "more" : MessageLookupByLibrary.simpleMessage("More"),
    "nameHint" : MessageLookupByLibrary.simpleMessage("Name"),
    "passwordHint" : MessageLookupByLibrary.simpleMessage("Password"),
    "register" : MessageLookupByLibrary.simpleMessage("Register"),
    "showProducts" : MessageLookupByLibrary.simpleMessage("Show Products"),
    "signInButton" : MessageLookupByLibrary.simpleMessage("Sign In"),
    "signUpButton" : MessageLookupByLibrary.simpleMessage("Sign Up"),
    "signUpScreen" : MessageLookupByLibrary.simpleMessage("SignUp Screen"),
    "topProducts" : MessageLookupByLibrary.simpleMessage("Top Products"),
    "userNameHint" : MessageLookupByLibrary.simpleMessage("User Name"),
    "welcomeMessage" : MessageLookupByLibrary.simpleMessage("Welcome To Our Market App"),
    "yourCartIsEmpty" : MessageLookupByLibrary.simpleMessage("Your Cart Is Empty")
  };
}
