import 'package:first_app/screens/login/login_screen.dart';
import 'package:first_app/utils/helper/language_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/services.dart';
import 'package:first_app/generated/l10n.dart';
void main() {
  runApp(ScopedModel<AppLanguage>(model: AppLanguage.instance, child: MyApp()));
}

// void setLocale(BuildContext context,Locale locale) async {
//   MaterialApp state=context.findAncestorStateOfType();
//  //  state.setLocals(locale);
// }
class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);


  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {


  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppLanguage>(
        builder: (context, child, model) =>  MaterialApp(
        home: LogInScreen(),
        theme: ThemeData(
          primaryColor: Colors.blue[900],
          accentColor: Colors.grey.shade800,
          brightness: Brightness.light,
          fontFamily: 'Satisfy',
          cardColor: Colors.amber[50],
        ),
        locale: AppLanguage.instance.appLocal,
         supportedLocales:
         [

           Locale('en', 'US'),
           Locale('ar', 'SA')
        ],
          // S.delegate.supportedLocales,
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return locale;
            }
          }
          return supportedLocales.first;
        },
      ),
    );
  }
}
