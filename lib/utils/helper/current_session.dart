import 'package:first_app/models/product.dart';
import 'package:first_app/models/user.dart';
import 'package:first_app/services/responses_model/registration_response.dart';

class CurrentSession{
  static CurrentSession instance=CurrentSession();
  User user;
  List<Product>cartProducts=[];
}