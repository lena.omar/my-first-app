import 'package:first_app/utils/ui/app_bar_helper.dart';
import 'package:first_app/utils/ui/common_widget_view.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:url_launcher/url_launcher.dart';

class LocationServices extends StatefulWidget {
  const LocationServices({Key key}) : super(key: key);

  @override
  _LocationServicesState createState() => _LocationServicesState();
}

class _LocationServicesState extends State<LocationServices> {
  var locationMessage = '';
  String latitude;
  String longitude;

  void getCurrentLocation() async {
    var position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    var lastPosition = await Geolocator().getLastKnownPosition();
    print("last position:$lastPosition");
    var lat = position.altitude;
    var long = position.longitude;


    //print("$lat,$long");

    setState(() {
      locationMessage = "$position";
    });
  }
  void googleMap() async {
    String googleUrl =
        "https://www.google.com/maps/search/?api=1&query=$latitude,$longitude";

    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else
      throw ("Couldn't open google maps");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBarHelper.instance
      //     .createAppBar(context: context, title: "Location Services"),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.location_on,
              color: Theme.of(context).primaryColor,
              size: 46,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "Get User Location",
              style: TextStyle(fontSize: 26),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "position:$locationMessage",
              style: TextStyle(
                  fontSize: 18, color: Theme.of(context).primaryColor),
            ),
            CommonUiViews.instance.creatCustomButton(
                context: context,
                title: "get Current Location",
                size: 20,
                onPressed: () {
                  getCurrentLocation();
                }),
            CommonUiViews.instance.creatCustomButton(
                context: context,
                title: "Open GoogleMap",
                size: 20,
                onPressed: () {
                  googleMap();
                }),

          ],
        ),
      ),
    );
  }
}
